import re
import glob
import os
source = "/home/doan19/Videos/convertdartnettokitti/outputval/labels"
for filename in glob.glob(os.path.join(source, '*.txt')):
    with open(filename, "r") as f:
        contents = f.read()
    contents = re.sub(r'^4', 'four', contents, flags = re.MULTILINE)
    with open(filename, "w") as f:
        f.write(contents)
