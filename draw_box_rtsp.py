import cv2
import numpy as np 

url = 'file:///ws/intrusion_detections/khanh_hoa.mp4'
pts2 = [867,656,1382,656,1382,1060,867,1060]
pts = np.array([[867, 656], [1382, 656], 
                [1382, 1060], [867, 1060]] ,
               np.int32)
cap = cv2.VideoCapture(url)
while True:
    _, frame = cap.read()
    break

pts = pts.reshape((-1, 1, 2))
  
isClosed = True
  
# Blue color in BGR
color = (255, 0, 0)
  
# Line thickness of 2 px
thickness = 2
  
# Using cv2.polylines() method
# Draw a Blue polygon with 
# thickness of 1 px
image = cv2.polylines(frame, [pts], 
                      isClosed, color, thickness)
cv2.imwrite("polygon.jpg", frame)
